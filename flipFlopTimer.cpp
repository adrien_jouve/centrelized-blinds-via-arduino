#include "flipFlopTimer.h"

cFlipFlopTimer::cFlipFlopTimer( int pinDriver,
				int time,
				String name):
	m_pin(pinDriver),
	m_actionDelay(time),
	m_name(name)
{
  pinMode( pinDriver, OUTPUT );
  digitalWrite( pinDriver, HIGH );     // sets 0V on the output
}

cFlipFlopTimer::cFlipFlopTimer( int pinDriver,
				String name):
	cFlipFlopTimer(pinDriver, 1000, name)
{
}

// Send a command to the Garage
bool cFlipFlopTimer::action()
{
	digitalWrite( m_pin, LOW );      // sets 5V on the output
	delay(m_actionDelay);
	digitalWrite( m_pin, HIGH );      // sets 0V on the output

	return true;
}
