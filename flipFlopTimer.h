#ifndef CFlipFlopTimer_H
#define CFlipFlopTimer_H

#include <Arduino.h>

// This class manage garage door
class cFlipFlopTimer
{
  public:
	cFlipFlopTimer( int pinDriver,
			int time,
			String name );

	cFlipFlopTimer( int pinDriver,
				String name );

    bool action();

  protected:

    int m_pin;

    // actionDelay is the time of the button is maintainted push ON
    int m_actionDelay;

    // managed component
    String m_name;

};

#endif //CFlipFlopTimer_H
