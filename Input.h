#ifndef CINPUT_H
#define CINPUT_H

#include "Controller.h"
#include "MotorList.h"
#include "Button.h"

// Manage input blind command, which can be up or down
class cInput
{
  public:
    cInput( int pinBoutonUp, int pinBoutonDown, cController& controllerRef, cMotorList::motorList emplacement );

    // should be call periodically, to verify if an action is needed
    void poll();

  protected:

    // manage push button
    void action( cStateMachine::Event order );
    
    // Return the current time
    unsigned long getTime();
  
    // refence to the controller, which centraized and dedicate the order to each blinds
    cController& m_controller;
  
    // where the blind is installed
    cMotorList::motorList m_emplacement;

    // button Up
    cButton* m_buttonUp;
  
    // button Down
    cButton* m_buttonDown;

    // flag to memorized push button
    bool m_holdUp;
    bool m_holdDown;

    // count to know the number of button press
    int m_pressCounter;

    // The previous action, to enable the stop request
    cStateMachine::Event m_previousOrdre;
    
    // time const to manage some things, in ms
    static const long m_timeLongPress = 1500;  // summer mode
    static const long m_timebeforeReset = 3000;  // reset the counter

    // All variable to manage the reset (based on a timer)
    unsigned long m_timeStart;
  
};

#endif //CINPUT_H
