#include "Button.h"
#include "blinds.h"

cButton::cButton( int pin ):
  m_tmpTime(0),
  m_endTimeValue(0),
  m_timeAppui(0),
  m_pin(pin),
  m_isTimeInTake( false ),
  m_timeMemory(0)
{
  pinMode( m_pin, INPUT );
}


bool cButton::buttonManagement( bool& holdFlag )
{
  bool fin( false );

  if( isButtonPushed() )
  {
    // can be arrived multiple time here since the button is hold
    if( !holdFlag )
    {
      m_timeMemory = getTime();
      holdFlag = true;
    }
  }
  else if( holdFlag )
  {
    unsigned long timeEnd( 0 );
      
    holdFlag = false;
    timeEnd = getTime();
    m_timeAppui = timeEnd - m_timeMemory;
    m_timeMemory = 0;
    fin = true;
  }

  return fin;
}


bool cButton::isButtonPushed( )
{
  bool state( false );

  if(digitalRead( m_pin ) == HIGH)
  {
    startTimer(m_antiAliasingTime);  // can be called multiple time because there is a flag to not reset the value of the start time
    if(isTimerEnd())
    {
      state = true;
    }
    
  } else {
    resetTimer();
  }

  return state;
}


// Start the timer
int cButton::startTimer(int endValue)
{
  if( !m_isTimeInTake )
  {
    m_endTimeValue = endValue;
    m_tmpTime = getTime();
    m_isTimeInTake = true;
  }
  return 0;
}

// reset the timer
void cButton::resetTimer()
{
  m_endTimeValue = 0;
  m_tmpTime = 0;
  m_isTimeInTake = false;
}

// return true if we are at the requested wait time
bool cButton::isTimerEnd()
{
  bool atRequestedTime(false);
  unsigned long timeEnd(0);
  unsigned long timeVal(0);
  
  timeEnd = getTime();
  timeVal = timeEnd - m_tmpTime;

  // if we are at the requested wait time value
  // then we free the timer and return true
  if(timeVal >= m_endTimeValue)
  {
    atRequestedTime = true;
  }

  return atRequestedTime;
}

unsigned long cButton::getTime()
{
  return millis();
}


