#include "blinds.h"
#include "comBlindDispatcher.h"
#include "StateMachine.h"
#include "MotorList.h"


cComBlindDispatcher::cComBlindDispatcher( cController &controller ):
	m_controller(controller)
{
}

// Run to manage action received from the communication link
void cComBlindDispatcher::run( String strRequester, String strOrder )
{
	cStateMachine::Event order = cStateMachine::stop;
	cMotorList::motorList requester = cMotorList::eBuanderie;

	if (strOrder.equals("up"))
		order = cStateMachine::up;
	else if (strOrder.equals("down"))
		order = cStateMachine::down;
	//else
		// manage return JSON Error

	// Convert Json data into local enum
	if (strRequester.equals("Buanderie"))
		requester = cMotorList::eBuanderie;

	else if(strRequester.equals("Cuisine"))
		requester = cMotorList::eCuisine;

	else if(strRequester.equals("Baie Ouest"))
			requester = cMotorList::eBaieOuest;

	else if(strRequester.equals("Baie Sud"))
			requester = cMotorList::eBaieSud;

	else if(strRequester.equals("Fixe"))
			requester = cMotorList::eFixe;

	else if(strRequester.equals("Bureau"))
			requester = cMotorList::eBureau;

	else if(strRequester.equals("Chambre Bebe"))
			requester = cMotorList::eChambreBebe;

	else if(strRequester.equals("Chambre Tilio"))
			requester = cMotorList::eChambreTilio;

	else if(strRequester.equals("Chambre Parents"))
			requester = cMotorList::eChambreParents;
	//else
		// manage return JSON Error

	m_controller.blind(order, requester);

}
