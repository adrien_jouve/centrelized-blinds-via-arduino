#ifndef TIMER_H
#define TIMER_H

#include "MotorList.h"


class cTimer
{
	public:
		cTimer(){};

		// return true if the timer is end
		bool update(cMotorList::motorList id);

		void start(cMotorList::motorList id, int duration);
		void stop(cMotorList::motorList id);

	protected:

		bool isActive(cMotorList::motorList id);
		void active(cMotorList::motorList id);
		void desactive(cMotorList::motorList id);

		unsigned long endTime[cMotorList::eSize];
		bool timerActivation[cMotorList::eSize];

};
#endif //TIMER_H
