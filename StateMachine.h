#ifndef CSTATEMACHINE_H
#define CSTATEMACHINE_H

#include "Timer.h"
#include "syslog.h"


// Manage the state and command the output to up or down the blind
class cStateMachine
{
  public:
    cStateMachine(int pinUp,
    		int pinDown,
			unsigned long int totalDurationMovement,
			String name,
			cMotorList::motorList motor,
			cTimer timer);

    typedef enum State{
      Mounting,
      Upped,
      Downhill,
      Downed,
      Middle
    }state;

    typedef enum Event{
      up,
      down,
      stop
    }event;

    typedef enum Action{
          Start,
          Stop
	}action;

    // Entry point
    State run(Event newEvent);

    // return blind state
    State getState(){ m_statusChange = 0; return m_state; };

    // Return 0 is no changes in the state machine else 1
    int isStatusChange(){ return m_statusChange; };

    // start the timer, should be call peridically
    void runTimer();

    // Return the string of corresponding state
    static String getStateString(State state);
    // Return the string of corresponding event
    static String getEventString(Event event);

  protected :

    // All handle method
    void handleMounting(Event event);
    void handleUp(Event event);
    void handleDownhill(Event event);
    void handleDown(Event event);
    void handleMiddle(Event event);

    // stop the blind by switch off the power
    void stopBlind();

    void downBlind();
    void upBlind();

    void startTimer();
    
    // different time to manage blind position
    unsigned long int m_totalDurationMovement;
    
    // blind state
    State m_state;
    
    // Status n-1
    State m_stateMinusOne;

    // Status change state
    int m_statusChange;

    // Pin where up wire is connected
    int m_pinUp;
  
    // Pin where down wire is connected
    int m_pinDown;

    // State machine related motor name
    String m_name;


	cMotorList::motorList m_motor;

    cTimer m_timer;

    // end order due to end of the timer
    bool m_endTimer;
};
#endif //CINPUT_H
