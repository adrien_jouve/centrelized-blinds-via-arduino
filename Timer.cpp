#include "Timer.h"
#include "blinds.h"


bool cTimer::update(cMotorList::motorList id){
	if (isActive(id) && (millis() >= endTime[id])){
		desactive(id);
		return true;
	}
	return false;
}

void cTimer::start(cMotorList::motorList id, int duration){
	int durationInMs = duration * 1000;
	endTime[id] = millis() + durationInMs;
	active(id);
}

void cTimer::stop(cMotorList::motorList id){
	desactive(id);
}

bool cTimer::isActive(cMotorList::motorList id)
{
	return timerActivation[id];
}

void cTimer::active(cMotorList::motorList id)
{
	timerActivation[id] = true;
}

void cTimer::desactive(cMotorList::motorList id)
{
	timerActivation[id] = false;
}
