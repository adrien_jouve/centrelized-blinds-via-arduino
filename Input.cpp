#include "Input.h"
#include "StateMachine.h"
#include "syslog.h"

#include "MotorList.h"  //just for motor name
#include "blinds.h"

const String cMotorList::motorName[]
{
  "Buanderie",
  "Cuisine",
  "BaieOuest",
  "BaieSud",
  "Fixe",
  "Bureau",
  "ChambreBebe",
  "ChambreTilio",
  "ChambreParents",
};

cInput::cInput( int pinBoutonUp,
                int pinBoutonDown,
                cController& controllerRef,
                cMotorList::motorList emplacement ):
    m_controller( controllerRef ),
    m_emplacement( emplacement ),
    m_holdUp( false ),
    m_holdDown( false ),
    m_pressCounter( 0 ),
    m_previousOrdre( cStateMachine::stop ),
	m_timeStart(0)
{
  m_buttonUp = new cButton( pinBoutonUp );
  m_buttonDown = new cButton( pinBoutonDown );
}


void cInput::poll()
{
  bool finAppuiUp( false );
  bool finAppuiDown( false );


  // UP button management
  if( !m_holdDown )
  {
    finAppuiUp = m_buttonUp->buttonManagement( m_holdUp );
    if( finAppuiUp )
    {
      syslog.logPrint((String)("\ncInput::poll: \t\t\t\t\t\t\t\t" + cMotorList::motorName[m_emplacement] + ": end of press button UP"), syslog.eDebug);
      action( cStateMachine::up );
      m_holdUp = false;
    }
  }

  // DOWN button management
  if( !m_holdUp )
  {
    finAppuiDown = m_buttonDown->buttonManagement( m_holdDown );

    if( finAppuiDown )
    {
      syslog.logPrint((String)("\ncInput::poll: \t\t\t\t\t\t\t\t" + cMotorList::motorName[m_emplacement] + ": end of press button DOWN"), syslog.eDebug);
      action( cStateMachine::down );
      m_holdDown = false;
    }
  }

  // Reset counter
  if( m_pressCounter != 0 )
  {
    unsigned long inactivityTime = getTime() - m_timeStart;
    if( (inactivityTime > 0) && (inactivityTime >= m_timebeforeReset) )
    {
      m_pressCounter = 0;
      syslog.logPrint((String)("cInput::poll: \t\t\t\t\t\t\t\t" + cMotorList::motorName[m_emplacement] + ": Counter reset Due to Timer"), syslog.eDebug);
    }
  }
}

void cInput::action( cStateMachine::Event order )
{
  // Get the time (to manage the reset)
  m_timeStart = getTime();

  // if that's a new action So reset the counter
  if( m_previousOrdre != order && m_pressCounter > 0 )
  {
    m_pressCounter = 0;
    m_controller.blind( cStateMachine::stop, m_emplacement );
    syslog.logPrint((String)("cInput::action: \t\t\t\t\t\t\t" + cMotorList::motorName[m_emplacement] + ": Button Counter reset"), syslog.eDebug);

  // Add one to the counter and launch action to the current blind OR if we are in the middle (example down, up (to stop), up (to up) in that case we don't want to call all other blinds)
  }else if( m_pressCounter == 0 || m_controller.getState(m_emplacement) == cStateMachine::Middle )    // Command request to the current blind
  {
    syslog.logPrint((String)("cInput::action: \t\t\t\t\t\t\t" + cMotorList::motorName[m_emplacement] + ": ONLY ONE order from "), syslog.eDebug);
    m_pressCounter++;
    m_controller.blind( order, m_emplacement );

  }else if( m_pressCounter == 1 ){    // Command request to the part of blinds
    syslog.logPrint((String)("cInput::action: \t\t\t\t\t\t\t" + cMotorList::motorName[m_emplacement] + ": PART order from "), syslog.eDebug);
    m_pressCounter++;
    m_controller.part( order, m_emplacement );

  }else if( m_pressCounter == 2 ){    // Command request to all blinds
    syslog.logPrint((String)("cInput::action: \t\t\t\t\t\t\t" + cMotorList::motorName[m_emplacement] + ": ALL order from "), syslog.eDebug);
    m_pressCounter++;
    m_controller.all( order );

  }else{
    m_pressCounter = 0;
  }

  m_previousOrdre = order;
}

// Return the current time
unsigned long cInput::getTime()
{
  return millis();
}
