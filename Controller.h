#ifndef CCONTROLLER_H
#define CCONTROLLER_H

#include "MotorList.h"
#include "Motor.h"

// cette classe gére les flux de controle
// Est appelé par Input puis gère l'information
// pour la transmettre aux bons Motors
class cController
{
  public:
  
    cController( ){};

    static const String motorCommand[2];

    // Commande d'un seul volet
    void blind( cStateMachine::Event order, cMotorList::motorList demandeur );

    // Commande de la partie (jours  ou nuit) dont le demandeur fait partie
    void part( cStateMachine::Event order, cMotorList::motorList demandeur );

    // Commande de tous les volets
    void all( cStateMachine::Event order );

    // return the state of the motor
    cStateMachine::State getState( cMotorList::motorList demandeur );

    // return true if one state of blinds has changed
    bool isStatusChange();

    // Gestion du timer, utilisé pour les callbacks de mouvement
    void runtimer();

    // movement duration to disable the power on the blind at the end for the 2m high blinds
    static const long movementTimeForHigher;
    // movement duration to disable the power on the blind at the end for the 1m high blinds
    static const long movementTimeForLower;

    // movement duration to obtainthe summer mode for the 2m high blinds
    static const long summerMovementTimeForHigher;
    // movement duration to obtainthe summer mode for the 1m high blinds
    static const long summerMovementTimeForLower;


};

#endif //CCONTROLLER_H
