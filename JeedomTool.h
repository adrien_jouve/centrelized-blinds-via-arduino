#ifndef CJeedomTool_H
#define CJeedomTool_H

#include <Arduino.h>

// This class manage garage door
class cJeedomTool
{
  public:
	cJeedomTool( ){};

    int getPin(char data[]);
    int getAction(char data[], int size);

};

#endif //CJeedomTool_H
