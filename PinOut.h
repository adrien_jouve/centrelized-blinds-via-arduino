#ifndef PINOUT_H
#define PINOUT_H

class cPinOut
{
	public:
		// Pinout des relais de commande des moteur de volet
    // BUANDERIE
    static const int pinBoutonMonteeBuanderie = 16;
    static const int pinBoutonDescenteBuanderie = 17;
    static const int pinCommandMonteeBuanderie = 36;
    static const int pinCommandDescenteBuanderie = 37;
    // CUISINE
    static const int pinBoutonMonteeCuisine = 18;
    static const int pinBoutonDescenteCuisine = 19;
    static const int pinCommandMonteeCuisine = 38;
    static const int pinCommandDescenteCuisine = 39;
    // BAIE OUEST
    static const int pinBoutonMonteeBaieOuest = 22;
    static const int pinBoutonDescenteBaieOuest = 23;
    static const int pinCommandMonteeBaieOuest = 40;
    static const int pinCommandDescenteBaieOuest = 41;
    // BAIE SUD
    static const int pinBoutonMonteeBaieSud = 24;
    static const int pinBoutonDescenteBaieSud = 25;
    static const int pinCommandMonteeBaieSud = 42;
    static const int pinCommandDescenteBaieSud = 43;
    // FIXE
    static const int pinBoutonMonteeFixe = 26;
    static const int pinBoutonDescenteFixe= 27;
    static const int pinCommandMonteeFixe = 44;
    static const int pinCommandDescenteFixe= 45;
    // BUREAU
    static const int pinBoutonMonteeBureau = 28;
    static const int pinBoutonDescenteBureau = 29;
    static const int pinCommandMonteeBureau = 46;
    static const int pinCommandDescenteBureau = 47;
    // CHAMBRE SUD
    static const int pinBoutonMonteeChambreSud = 30;
    static const int pinBoutonDescenteChambreSud = 31;
    static const int pinCommandMonteeChambreSud = 48;
    static const int pinCommandDescenteChambreSud = 49;
    // CHAMBRE EST
    static const int pinBoutonMonteeChambreEst = 32;
    static const int pinBoutonDescenteChambreEst = 33;
    static const int pinCommandMonteeChambreEst = 51;
    static const int pinCommandDescenteChambreEst = 50;
    // CHAMBRE NORD
    static const int pinBoutonMonteeChambreNord = 34;
    static const int pinBoutonDescenteChambreNord = 35;
    static const int pinCommandMonteeChambreNord = 52;
    static const int pinCommandDescenteChambreNord = 53;
};
#endif //PINOUT_H
