#include "JeedomTool.h"


int cJeedomTool::getPin(char data[]){

    // Pin val calculation
    data[1] = data[1]-'0';
    data[2] = data[2]-'0';
    data[3] = data[3]-'0';

    return 100 * int(data[1]) + 10 * int(data[2]) + int(data[3]);
}


int cJeedomTool::getAction(char data[], int size){

    // Action value calculation
    // Find how many digit is received as action
	  int pinAction(0);
	  int exp(1);

    for(int val=5 ; val <= size-1 ; val ++) {
		exp = exp * 10;
    }

    for(int val=4 ; val <= size-1 ; val ++) {
    	data[val] = data[val]-'0';
		pinAction = pinAction + int(data[val]) * exp;
		exp = exp / 10;
    }

    return pinAction;
}
