# Blinds manager Via Arduino Mega #

# Version 4.0.0 #

* Fully interfaced to Jeedom using Jeeduino plugin
* Fix issue regarding the state machin
* Add status blinds feedback to jeedom even if the user use the physical button

# Version 3.0.0 #

## Bugfix##

* after a period of time the arduino is block, because of the serial printf which try to load message on the communication link without client, so the serial buffer overload. To solve just remove serial printing in Release
* Each 24 hours we reset the arduino to prevent object unallocated

# Version 2.0.0 #

## Summary ##

### Basic functioning ###
This software manage all house blinds.
Based on simple push button and wired blinds.
The push button is managed smartly:

* First push, close or open the blind

* Second push on the same button, close or open a group of blinds (day part or night part)

* Third push on the same button, close or open all house's blinds

After 10sec the push counter is reseted.

## How to set up? ##

* compile the soft via arduino compiler
* load the complied soft on your arduino
* lets play


## Contribution guidelines ##

You can contribute to this project using pull request. Thanks in advance.

If you find some bugs, please fill free to log it.

## Which libraries you need ##

For the run time, you need to install the timerObject (to have a callback on a preset timer):
http://playground.arduino.cc/Code/ArduinoTimerObject
For unit test:
https://github.com/mmurdoch/arduinounit

## Basic adaption to your project ##

### Modify input ###

In PinOut.h you can declare your own pinout configuration.
For one blind you need:

* up button

* down button

* up motor action (of the blind)

* down motor action (of the blind)

### Create your own button array ###
* In centralisation you need to modify the declaration array where all buttons are declared.

### Create your own motor list ###
* In Controller.cpp, you need to create all stop methods, one stop method per motor.
* In Controller.cpp, modify the motor array to declare your own one.
* Declare all blinds name in MotorList.h

### How to use Eclipse to compile Arduino program ###
* just download sloeder form: http://eclipse.baeyens.it/how_to.shtml
* install the complet software
All the rest are done automaticaly
