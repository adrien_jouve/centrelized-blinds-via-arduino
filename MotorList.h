#ifndef MOTORLIST_H
#define MOTORLIST_H

#include "blinds.h"

class cMotorList
{
  public:
    typedef enum _motorList
    {
      eBuanderie,
      eCuisine,
      eBaieOuest,
      eBaieSud,
      eFixe,
      eBureau,
      eChambreBebe,
      eChambreTilio,
      eChambreParents,
      
      eSize
    }motorList;
    
    static const String motorName[9];
};
#endif //MOTORLIST_H
