#ifndef CSYSLOG_H
#define CSYSLOG_H


// This class is used to manage a button
class cSyslog
{
  public:
    cSyslog(){
      if(m_index >= m_logMaxSize) {
        m_index = 0;
      }
    };

    typedef enum eLogLevel{
      eError = 0,
      eWarning,
      eDebug
    }eLogLevel;
    
    void logPrint(String logData, eLogLevel level){
      m_logBuffer[m_index] = getLevelString(level) + logData;

      m_index++;
      if(m_index >= m_logMaxSize) {
        m_index = 0;
      }
    };

    void get() {
      for( int i; i <= m_logMaxSize; i++) {
        Serial.println(m_logBuffer[i]);
      }
    };


  protected:

    static const int m_logMaxSize = 70;
    
    String m_logBuffer[m_logMaxSize];

    int m_index;

    String getLevelString(eLogLevel lvl) {
      switch (lvl){
        case eError:
          return "ERROR:   ";
        case eWarning:
          return "WARNING: ";
        case eDebug:
          return "DEBUG:   ";
        default:
          break;
      }

      return "";
    }

};

// Declare the unique logger here
static cSyslog syslog;

#endif //CSYSLOG_H
