#include "Controller.h"
#include "PinOut.h"
#include "StateMachine.h"
#include "blinds.h"
#include "Timer.h"
#include "syslog.h"


// Object Timer ONE for all the house
cTimer timer;

// movement duration to disable the power on the blind at the end for the 2m high blinds
const long cController::movementTimeForHigher = 25;  // in sec
// movement duration to disable the power on the blind at the end for the 1m high blinds
const long cController::movementTimeForLower = 17; // in sec

// movement duration to obtainthe summer mode for the 2m high blinds
const long cController::summerMovementTimeForHigher = 21;  // in sec
// movement duration to obtainthe summer mode for the 1m high blinds
const long cController::summerMovementTimeForLower = 12;  // in sec


// motors list, which should be in the same sequence as MotorList enum
static cMotor motor[cMotorList::eSize] =
{
  cMotor( cPinOut::pinCommandMonteeBuanderie,   cPinOut::pinCommandDescenteBuanderie,   cController::movementTimeForHigher, cController::summerMovementTimeForHigher, "Buanderie", 			cMotorList::eBuanderie, 		timer ),
  cMotor( cPinOut::pinCommandMonteeCuisine,     cPinOut::pinCommandDescenteCuisine,     cController::movementTimeForLower,  cController::summerMovementTimeForLower,  "Cuisine", 			cMotorList::eCuisine, 			timer ),
  cMotor( cPinOut::pinCommandMonteeBaieOuest,   cPinOut::pinCommandDescenteBaieOuest,   cController::movementTimeForHigher, cController::summerMovementTimeForHigher, "Baie Ouest", 		cMotorList::eBaieOuest, 		timer ),
  cMotor( cPinOut::pinCommandMonteeBaieSud,     cPinOut::pinCommandDescenteBaieSud,     cController::movementTimeForHigher, cController::summerMovementTimeForHigher, "Baie Sud", 			cMotorList::eBaieSud, 			timer ),
  cMotor( cPinOut::pinCommandMonteeFixe,        cPinOut::pinCommandDescenteFixe,        cController::movementTimeForHigher, cController::summerMovementTimeForHigher, "Fixe", 				cMotorList::eFixe, 				timer ),
  cMotor( cPinOut::pinCommandMonteeBureau,      cPinOut::pinCommandDescenteBureau,      cController::movementTimeForLower,  cController::summerMovementTimeForLower,  "Bureau", 			cMotorList::eBureau, 			timer ),
  cMotor( cPinOut::pinCommandMonteeChambreSud,  cPinOut::pinCommandDescenteChambreSud,  cController::movementTimeForLower,  cController::summerMovementTimeForLower,  "Chambre enfant", 	cMotorList::eChambreBebe, 		timer ),
  cMotor( cPinOut::pinCommandMonteeChambreEst,  cPinOut::pinCommandDescenteChambreEst,  cController::movementTimeForLower,  cController::summerMovementTimeForLower,  "Chambre Tilio", 		cMotorList::eChambreTilio, 		timer ),
  cMotor( cPinOut::pinCommandMonteeChambreNord, cPinOut::pinCommandDescenteChambreNord, cController::movementTimeForLower,  cController::summerMovementTimeForLower,  "Chambre Parents", 	cMotorList::eChambreParents, 	timer )
};


void cController::blind( cStateMachine::Event order, cMotorList::motorList demandeur )
{
    syslog.logPrint((String)("cController::blind: \t\t\t\t\t" + cMotorList::motorName[demandeur] + ": action order " + cStateMachine::getEventString(order)), syslog.eDebug);

    motor[demandeur].action( order );
}


void cController::part( cStateMachine::Event order, cMotorList::motorList demandeur )
{
  int index( 0 );
  
  // PARTIE JOUR
  if( demandeur <= cMotorList::eFixe )
  {
    for( index = 0; index <= cMotorList::eFixe; index++ )
    {
      motor[index].action( order );
    }
  }
  // PARTIE NUIT
  else
  {
    for( index = cMotorList::eFixe+1; index < cMotorList::eSize; index++ )
    {
      motor[index].action( order );
    }
  }
}


void cController::all( cStateMachine::Event order )
{
  int index( 0 );
  
  for( index = 0; index < cMotorList::eSize; index++ )
  {
    motor[index].action( order );
  }
}

// return the state of the motor
cStateMachine::State cController::getState( cMotorList::motorList demandeur ){
  return motor[demandeur].getState();
}

// return true if one state of blinds has changed
bool cController::isStatusChange(){
	bool ret(false);
	int isStatusChange(0);

	for( int index = 0; index < cMotorList::eSize; index++ ){
		isStatusChange = isStatusChange | motor[index].isStatusChange();
	}
 
	if( isStatusChange == 1){
		ret = true;
	}

	return ret;
}

void cController::runtimer()
{
  for( int index = 0; index < cMotorList::eSize; index++ )
  {
    motor[index].runTimer();
  }
}
