#ifndef CBUTTON_H
#define CBUTTON_H

#include "MotorList.h"

// This class is used to manage a button
class cButton
{
  public:
    cButton( int pin );

    // Push button management
    bool buttonManagement( bool& holdFlag );

    // Verify input button state
    bool isButtonPushed( );
    
    // Start the timer
    int startTimer(int endValue);
    
    // reset the timer
    void resetTimer();

    // return true if we are at the requested wait time
    bool isTimerEnd();

    // return the time duration of the press button
    unsigned long getPressButtonDuration(){
      return m_timeAppui;
    }

  protected:
    // return time is seconds
    unsigned long getTime();

    // local saved time
    unsigned long m_tmpTime;

    // time of the end timer
    unsigned long m_endTimeValue;
        
    // hold button duration
    unsigned long m_timeAppui;

    // input pin of button
    int m_pin;

    // under time capture
    bool m_isTimeInTake;

    // hold push button time
    unsigned long m_timeMemory;

    // anti aliasing time
    static const unsigned long m_antiAliasingTime = 150;
};
#endif //CBUTTON_H
