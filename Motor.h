#ifndef CMOTEUR_H
#define CMOTEUR_H

#include "StateMachine.h"

// This class is in charge of one blind management
class cMotor
{
  public:
    cMotor( int pinUp,
    		int pinDown,
			unsigned long int totalDurationMovement,
			unsigned long int summerDurationMovement,
			String name,
			cMotorList::motorList motor,
			cTimer timer );

    // manage all action (up, down ...)
    bool action(cStateMachine::Event event);
    bool stop();

    void runTimer();

    // return blind state
    cStateMachine::State getState(){ return m_stateMachine->getState(); };

    // Return 0 is no changes in the state machine else 1
    int isStatusChange();

  protected:
    // State machine
    cStateMachine* m_stateMachine;

    unsigned long int m_summerDurationMovement;
  
};

#endif //CMOTEUR_H
