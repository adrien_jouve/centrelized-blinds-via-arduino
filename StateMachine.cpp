#include "blinds.h"
#include "StateMachine.h"

cStateMachine::cStateMachine( int pinUp,
                int pinDown,
                unsigned long int totalDurationMovement,
				String name,
				cMotorList::motorList motor,
				cTimer timer):
  m_totalDurationMovement( totalDurationMovement ),
  m_state(Middle),
  m_stateMinusOne(Middle),
  m_statusChange(0),
  m_pinUp(pinUp),
  m_pinDown(pinDown),
  m_name(name),
  m_motor(motor),
  m_timer(timer),
  m_endTimer(false)
{
}

cStateMachine::State cStateMachine::run(Event newEvent){
  syslog.logPrint((String)("cStateMachine::run: \t\t\t\t" + m_name + ": State Machine call with event " + getEventString(newEvent) + " and current state is " + getStateString(m_state) + "\n"), syslog.eDebug);

  switch( m_state ){
    case Mounting :
      handleMounting(newEvent);
    break;
    case Upped :
      handleUp(newEvent);
    break;
    case Downhill :
      handleDownhill(newEvent);
    break;
    case Downed :
      handleDown(newEvent);
    break;
    case Middle :
      handleMiddle(newEvent);
    break;
    default :
    break;
  }
  
  return m_state;
}


void cStateMachine::handleMounting(Event event){
  switch( event ){
    case stop :
      stopBlind();
      if( m_endTimer )
    	  m_state = Upped;
      else  // because we received a stop from the controller in some case (example down blind, stop by using button up, down again => without this we are blocked)
          m_state = Middle;
    break;
    case down :
      stopBlind();
      m_state = Middle;
    break;
    default:
#ifdef DEBUG
    	Serial.print("\nSM: mounting | state= ");
    	Serial.print(getStateString(m_state));
    	Serial.print(" event= ");
    	Serial.println(getEventString(event));
#endif
    break;
  }
}

void cStateMachine::handleUp(Event event){
  switch( event ){
    case down :
      stopBlind();
      downBlind();
      startTimer();
      m_state = Downhill;
    break;
    default:
#ifdef DEBUG
    	Serial.print("\nSM: up | state= ");
    	Serial.print(getStateString(m_state));
    	Serial.print(" event= ");
    	Serial.println(getEventString(event));
#endif
    break;
  }
}

void cStateMachine::handleDownhill(Event event){
  switch( event ){
    case stop :
      stopBlind();
      if( m_endTimer )
    	  m_state = Downed;
      else
    	  m_state = Middle;
    break;
    case up :
      stopBlind();
      m_state = Middle;
    break;
    default:
#ifdef DEBUG
    	Serial.print("\nSM: downhill | state= ");
    	Serial.print(getStateString(m_state));
    	Serial.print(" event= ");
    	Serial.println(getEventString(event));
#endif
    break;
  }
}

void cStateMachine::handleDown(Event event){
  switch( event ){
    case up :
      stopBlind();
      upBlind();
      startTimer();
      m_state = Mounting;
    break;
    default:
#ifdef DEBUG
    	Serial.print("\nSM: down | state= ");
    	Serial.print(getStateString(m_state));
    	Serial.print(" event= ");
    	Serial.println(getEventString(event));
#endif
    break;
  }
}

void cStateMachine::handleMiddle(Event event){
  switch( event ){
    case up :
      stopBlind();
      upBlind();
      startTimer();
      m_state = Mounting;
    break;
    case down :
      stopBlind();
      downBlind();
      startTimer();
      m_state = Downhill;
    break;
    default:
#ifdef DEBUG
    	Serial.print("\nSM: middle | state= ");
    	Serial.print(getStateString(m_state));
    	Serial.print(" event= ");
    	Serial.println(getEventString(event));
#endif
    break;
  }
}


// ##################### Action part #####################


void cStateMachine::runTimer()
{
  if( m_timer.update(m_motor)){
	  m_endTimer = true;
	  run(Event::stop);
  }

  // Update the state
  if( m_state != m_stateMinusOne ){
	  m_statusChange = 1;
	  m_stateMinusOne = m_state;
  }
}


// call only by the state machine
void cStateMachine::stopBlind() {
  syslog.logPrint((String)("cStateMachine::stopBlind: \t\t" + m_name + ": Stop the blind "), syslog.eDebug);
  
  // stop all movement
  digitalWrite( m_pinDown, HIGH );     // sets 0V on the output
  digitalWrite( m_pinUp, HIGH );      // sets 0V on the output

  // stop the timer
  m_timer.stop(m_motor);

  // delay for relay moving
  delay(250);
}

// call only by the state machine
void cStateMachine::downBlind() {
  syslog.logPrint((String)("cStateMachine::downBlind: \t" + m_name + ": Down the blind "), syslog.eDebug);

  digitalWrite( m_pinDown, LOW );      // sets 5V on the output

//  Serial.print("{\"blind\":\"" + m_name + ",\"state\":\"down\"}#");
}

// call only by the state machine
void cStateMachine::upBlind() {
  syslog.logPrint((String)("cStateMachine::downBlind: \t" + m_name + ": Up the blind "), syslog.eDebug);

  digitalWrite( m_pinUp, LOW );      // sets 5V on the output

//  Serial.print("{\"blind\":\"" + m_name + ",\"state\":\"up\"}#");
}

// call only by the state machine
void cStateMachine::startTimer()
{
  // start the timer
  m_timer.start(m_motor, m_totalDurationMovement);
  m_endTimer = false;
}


String cStateMachine::getStateString(State state)
{
	  switch( state ){
	    case Mounting :
	      return "Mounting";
	    break;
	    case Upped :
	    	return "Up";
	    break;
	    case Downhill :
	    	return "Downhill";
	    break;
	    case Downed :
	    	return "Down";
	    break;
	    case Middle :
	    	return "Middle";
	    break;
	    default :
	    break;
	  }
	  return "";
}

String cStateMachine::getEventString(Event event)
{
	  switch( event ){
	    case up :
	      return "UP";
	    break;
	    case down :
	    	return "DOWN";
	    break;
	    case stop :
	    	return "STOP";
	    break;
	    default :
	    break;
	  }
	  return "";
}
