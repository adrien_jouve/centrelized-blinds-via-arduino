#include "blinds.h"
#include "Motor.h"
#include "Timer.h"

cMotor::cMotor( int pinUp,
                int pinDown,
                unsigned long int totalDurationMovement,
                unsigned long int summerDurationMovement,
				String name,
				cMotorList::motorList motor,
				cTimer timer):
	m_summerDurationMovement(summerDurationMovement)
{
  m_stateMachine = new cStateMachine(pinUp, pinDown, totalDurationMovement, name, motor, timer);
  pinMode( pinUp, OUTPUT );
  pinMode( pinDown, OUTPUT );
  digitalWrite( pinDown, HIGH );     // sets 0V on the output
  digitalWrite( pinUp, HIGH );       // sets 0V on the output
}

// Call by the controller
bool cMotor::action(cStateMachine::Event event)
{
  // up order
  m_stateMachine->run( event );

  return true;
}

// Call by the controller
bool cMotor::stop()
{
  // up order
  action( cStateMachine::stop );

  return true;
}

void cMotor::runTimer()
{
  m_stateMachine->runTimer();
}

// Return 0 is no changes in the state machine else 1
int cMotor::isStatusChange(){
	return m_stateMachine->isStatusChange();
}



