#ifndef CStartStop_H
#define CStartStop_H

#include "StateMachine.h"

// This class manage a start stop device
// One command to activate the relay
// One command to desactivate the relay
class cStartStop
{
  public:
	cStartStop( int pinDriver,
			String name );

    bool action(cStateMachine::Action order);

  protected:

    int m_pin;

    // managed component
    String m_name;

};

#endif //CStartStop_H
