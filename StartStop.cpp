#include "StartStop.h"

cStartStop::cStartStop( int pinDriver,
				String name):
	m_pin(pinDriver),
	m_name(name)
{
  pinMode( pinDriver, OUTPUT );
  digitalWrite( pinDriver, HIGH );     // sets 0V on the output
}


// Send a command to the Garage
bool cStartStop::action(cStateMachine::Action order)
{
	bool ret(false);

	if( order == cStateMachine::Start ){
		digitalWrite( m_pin, LOW );      // sets 5V on the output
		ret = true;
	}
	else if( order == cStateMachine::Stop ){
		digitalWrite( m_pin, HIGH );      // sets 0V on the output
		ret = true;
	}
	else{
	    syslog.logPrint((String)(m_name + " Received wrong command, order = " + order), syslog.eError);
	}

	return ret;
}
