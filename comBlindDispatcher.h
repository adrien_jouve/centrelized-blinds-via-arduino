#ifndef CCOMBLINDSDISPATCHER_H
#define CCOMBLINDSDISPATCHER_H

#include "Controller.h"

// This class is in charge of one blind management
class cComBlindDispatcher
{
  public:
	cComBlindDispatcher( cController &controller );

	// Run to manage action received from the communication link
    void run( String requester, String order );

  protected:
    // Motor controller reference
    cController &m_controller;

};

#endif //CCOMBLINDSDISPATCHER_H
